#!/bin/bash

set -x

STREAM=~/kashyyyk/footage/nani/crash.mp4
TS="ts.txt"
ARGS=""

if [[ $1 == "last" ]] ; then
    ARGS+="-l"
fi

./slice.py $STREAM "$TS" $ARGS && vlc clip.mp4
