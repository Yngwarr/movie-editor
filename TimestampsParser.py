# Generated from Timestamps.g4 by ANTLR 4.9.3
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\16")
        buf.write("B\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\3\2\7\2\16\n")
        buf.write("\2\f\2\16\2\21\13\2\3\3\3\3\5\3\25\n\3\3\3\3\3\5\3\31")
        buf.write("\n\3\3\3\3\3\5\3\35\n\3\3\3\5\3 \n\3\3\4\7\4#\n\4\f\4")
        buf.write("\16\4&\13\4\3\5\3\5\5\5*\n\5\3\5\3\5\5\5.\n\5\3\5\3\5")
        buf.write("\3\6\3\6\5\6\64\n\6\3\6\3\6\5\68\n\6\3\6\3\6\3\6\5\6=")
        buf.write("\n\6\3\6\5\6@\n\6\3\6\3$\2\7\2\4\6\b\n\2\2\2H\2\17\3\2")
        buf.write("\2\2\4\24\3\2\2\2\6$\3\2\2\2\b\'\3\2\2\2\n?\3\2\2\2\f")
        buf.write("\16\5\4\3\2\r\f\3\2\2\2\16\21\3\2\2\2\17\r\3\2\2\2\17")
        buf.write("\20\3\2\2\2\20\3\3\2\2\2\21\17\3\2\2\2\22\23\7\7\2\2\23")
        buf.write("\25\7\16\2\2\24\22\3\2\2\2\24\25\3\2\2\2\25\26\3\2\2\2")
        buf.write("\26\30\5\b\5\2\27\31\7\16\2\2\30\27\3\2\2\2\30\31\3\2")
        buf.write("\2\2\31\37\3\2\2\2\32\34\7\3\2\2\33\35\7\16\2\2\34\33")
        buf.write("\3\2\2\2\34\35\3\2\2\2\35\36\3\2\2\2\36 \5\6\4\2\37\32")
        buf.write("\3\2\2\2\37 \3\2\2\2 \5\3\2\2\2!#\13\2\2\2\"!\3\2\2\2")
        buf.write("#&\3\2\2\2$%\3\2\2\2$\"\3\2\2\2%\7\3\2\2\2&$\3\2\2\2\'")
        buf.write(")\5\n\6\2(*\7\16\2\2)(\3\2\2\2)*\3\2\2\2*+\3\2\2\2+-\7")
        buf.write("\3\2\2,.\7\16\2\2-,\3\2\2\2-.\3\2\2\2./\3\2\2\2/\60\5")
        buf.write("\n\6\2\60\t\3\2\2\2\61\62\7\b\2\2\62\64\7\4\2\2\63\61")
        buf.write("\3\2\2\2\63\64\3\2\2\2\64\67\3\2\2\2\65\66\7\b\2\2\66")
        buf.write("8\7\4\2\2\67\65\3\2\2\2\678\3\2\2\289\3\2\2\29<\7\b\2")
        buf.write("\2:;\7\5\2\2;=\7\b\2\2<:\3\2\2\2<=\3\2\2\2=@\3\2\2\2>")
        buf.write("@\7\6\2\2?\63\3\2\2\2?>\3\2\2\2@\13\3\2\2\2\16\17\24\30")
        buf.write("\34\37$)-\63\67<?")
        return buf.getvalue()


class TimestampsParser ( Parser ):

    grammarFileName = "Timestamps.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'-'", "':'", "'.'", "'end'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "SOLO", "INT", "ID", "CYRILLIC", "PUNCTUATION", 
                      "COMMENT", "NEWLINE", "WS" ]

    RULE_info = 0
    RULE_timestamp = 1
    RULE_label = 2
    RULE_interval = 3
    RULE_time = 4

    ruleNames =  [ "info", "timestamp", "label", "interval", "time" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    SOLO=5
    INT=6
    ID=7
    CYRILLIC=8
    PUNCTUATION=9
    COMMENT=10
    NEWLINE=11
    WS=12

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.9.3")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class InfoContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def timestamp(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TimestampsParser.TimestampContext)
            else:
                return self.getTypedRuleContext(TimestampsParser.TimestampContext,i)


        def getRuleIndex(self):
            return TimestampsParser.RULE_info

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInfo" ):
                listener.enterInfo(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInfo" ):
                listener.exitInfo(self)




    def info(self):

        localctx = TimestampsParser.InfoContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_info)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 13
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << TimestampsParser.T__3) | (1 << TimestampsParser.SOLO) | (1 << TimestampsParser.INT))) != 0):
                self.state = 10
                self.timestamp()
                self.state = 15
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TimestampContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def interval(self):
            return self.getTypedRuleContext(TimestampsParser.IntervalContext,0)


        def SOLO(self):
            return self.getToken(TimestampsParser.SOLO, 0)

        def WS(self, i:int=None):
            if i is None:
                return self.getTokens(TimestampsParser.WS)
            else:
                return self.getToken(TimestampsParser.WS, i)

        def label(self):
            return self.getTypedRuleContext(TimestampsParser.LabelContext,0)


        def getRuleIndex(self):
            return TimestampsParser.RULE_timestamp

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTimestamp" ):
                listener.enterTimestamp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTimestamp" ):
                listener.exitTimestamp(self)




    def timestamp(self):

        localctx = TimestampsParser.TimestampContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_timestamp)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 18
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==TimestampsParser.SOLO:
                self.state = 16
                self.match(TimestampsParser.SOLO)
                self.state = 17
                self.match(TimestampsParser.WS)


            self.state = 20
            self.interval()
            self.state = 22
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==TimestampsParser.WS:
                self.state = 21
                self.match(TimestampsParser.WS)


            self.state = 29
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==TimestampsParser.T__0:
                self.state = 24
                self.match(TimestampsParser.T__0)
                self.state = 26
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,3,self._ctx)
                if la_ == 1:
                    self.state = 25
                    self.match(TimestampsParser.WS)


                self.state = 28
                self.label()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class LabelContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return TimestampsParser.RULE_label

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLabel" ):
                listener.enterLabel(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLabel" ):
                listener.exitLabel(self)




    def label(self):

        localctx = TimestampsParser.LabelContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_label)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 34
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,5,self._ctx)
            while _alt!=1 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1+1:
                    self.state = 31
                    self.matchWildcard() 
                self.state = 36
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,5,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class IntervalContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def time(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TimestampsParser.TimeContext)
            else:
                return self.getTypedRuleContext(TimestampsParser.TimeContext,i)


        def WS(self, i:int=None):
            if i is None:
                return self.getTokens(TimestampsParser.WS)
            else:
                return self.getToken(TimestampsParser.WS, i)

        def getRuleIndex(self):
            return TimestampsParser.RULE_interval

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInterval" ):
                listener.enterInterval(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInterval" ):
                listener.exitInterval(self)




    def interval(self):

        localctx = TimestampsParser.IntervalContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_interval)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 37
            self.time()
            self.state = 39
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==TimestampsParser.WS:
                self.state = 38
                self.match(TimestampsParser.WS)


            self.state = 41
            self.match(TimestampsParser.T__0)
            self.state = 43
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==TimestampsParser.WS:
                self.state = 42
                self.match(TimestampsParser.WS)


            self.state = 45
            self.time()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TimeContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INT(self, i:int=None):
            if i is None:
                return self.getTokens(TimestampsParser.INT)
            else:
                return self.getToken(TimestampsParser.INT, i)

        def getRuleIndex(self):
            return TimestampsParser.RULE_time

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTime" ):
                listener.enterTime(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTime" ):
                listener.exitTime(self)




    def time(self):

        localctx = TimestampsParser.TimeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_time)
        self._la = 0 # Token type
        try:
            self.state = 61
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [TimestampsParser.INT]:
                self.enterOuterAlt(localctx, 1)
                self.state = 49
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,8,self._ctx)
                if la_ == 1:
                    self.state = 47
                    self.match(TimestampsParser.INT)
                    self.state = 48
                    self.match(TimestampsParser.T__1)


                self.state = 53
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,9,self._ctx)
                if la_ == 1:
                    self.state = 51
                    self.match(TimestampsParser.INT)
                    self.state = 52
                    self.match(TimestampsParser.T__1)


                self.state = 55
                self.match(TimestampsParser.INT)
                self.state = 58
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==TimestampsParser.T__2:
                    self.state = 56
                    self.match(TimestampsParser.T__2)
                    self.state = 57
                    self.match(TimestampsParser.INT)


                pass
            elif token in [TimestampsParser.T__3]:
                self.enterOuterAlt(localctx, 2)
                self.state = 60
                self.match(TimestampsParser.T__3)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





