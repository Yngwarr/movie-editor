grammar Timestamps;

info : timestamp* ;

timestamp : (SOLO WS)? interval WS? ('-' WS? label)? ;
label : .*? ;
interval : time WS? '-' WS? time ;
time
    : (INT ':')? (INT ':')? INT ('.' INT)?
    | 'end' ;

SOLO: [Ss] ;
INT: [0-9]+ ;
ID: [a-zA-Z_]+ ;
CYRILLIC: [а-яА-ЯёЁ]+ ;
PUNCTUATION: [!@#$%^&*()=+_,.?] | '-';

COMMENT : '#' .*? NEWLINE -> channel(HIDDEN);
NEWLINE: '\r'? '\n' -> channel(HIDDEN);
WS : [ \t]+ ;
