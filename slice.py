#!/usr/bin/env python

from sys import argv
import math
import argparse

from moviepy.editor import *
from PIL import Image
import numpy as np

from parser import *


def parseArgs():
    parser = argparse.ArgumentParser(description='Slice your videos into pieces!')
    parser.add_argument('video', help='a videofile to slice')
    parser.add_argument('timestamps', help='a description file')
    parser.add_argument('-l', '--last', action='store_true', help='render only the last clip in file')
    parser.add_argument('-d', '--debug', action='store_true', help='print debug info on clips')
    return parser.parse_args()


def parseTS(lines):
    res = []

    for l in lines:
        if l[0] == '#': continue
        if len(l.strip()) == 0: continue
        res.append(tuple([x.strip() for x in l.split('-', 2)]))

    return res


def zoom_in(clip, zoom_ratio=0.04):
    def effect(get_frame, t):
        img = Image.fromarray(get_frame(t))
        base_size = img.size

        new_size = [
            math.ceil(img.size[0] * (1 + (zoom_ratio * t))),
            math.ceil(img.size[1] * (1 + (zoom_ratio * t)))
        ]

        # The new dimensions must be even.
        new_size[0] += new_size[0] % 2
        new_size[1] += new_size[1] % 2

        img = img.resize(new_size, Image.LANCZOS)

        x = math.ceil((new_size[0] - base_size[0]) / 2)
        y = math.ceil((new_size[1] - base_size[1]) / 2)

        img = img.crop([
            x, y, new_size[0] - x, new_size[1]
        ]).resize(base_size, Image.LANCZOS)

        result = np.array(img)
        img.close()

        return result

    return clip.fl(effect)


def addDebugText(clip, text):
    info = TextClip(text, fontsize=28, color='white').set_pos((0, 0))
    return CompositeVideoClip([clip, info]).set_duration(clip.duration)


def cut(stream, ts, debug):
    print(f'processing {ts}')
    sub = stream.subclip(ts.t0, ts.t1)

    mod = sub
    if ts.label == 'bossmusic':
        fading = sub.audio.audio_fadeout(sub.duration)
        music = AudioFileClip('boss_music.m4a').volumex(0.7).audio_fadein(sub.duration)
        mod1 = sub.set_audio(CompositeAudioClip([fading, music]).set_duration(sub.duration))
        mod = zoom_in(mod1)
    elif ts.label == 'bossmusic_intense':
        fading = sub.audio.audio_fadeout(sub.duration)
        music = AudioFileClip('boss_music.m4a').volumex(0.7).audio_fadein(sub.duration)
        mod1 = sub.set_audio(CompositeAudioClip([fading, music]).set_duration(sub.duration))
        mod = zoom_in(mod1, 0.09)
    elif ts.label == 'close-up':
        w = 615
        h = 346
        mod = sub.crop(x1=sub.w - w, width=w, y1=686, height=h).resize(width=sub.w)

    return mod if not debug else addDebugText(mod, f'{ts.t0} - {ts.t1}')


def main():
    args = parseArgs()

    streamFile = args.video
    tsFile = args.timestamps

    ts = parseTimestamps(tsFile)

    stream = VideoFileClip(streamFile)
    video = None

    if args.last and not ts.has_solo:
        video = cut(stream, ts.cmds[-1], debug=args.debug)
    else:
        clips = []
        for t in ts.to_compose():
            clips.append(cut(stream, t, debug=args.debug))
        video = concatenate_videoclips(clips)

    video.write_videofile('clip.mp4')


if __name__ == '__main__':
    main()
