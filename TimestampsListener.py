# Generated from Timestamps.g4 by ANTLR 4.9.3
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .TimestampsParser import TimestampsParser
else:
    from TimestampsParser import TimestampsParser

# This class defines a complete listener for a parse tree produced by TimestampsParser.
class TimestampsListener(ParseTreeListener):

    # Enter a parse tree produced by TimestampsParser#info.
    def enterInfo(self, ctx:TimestampsParser.InfoContext):
        pass

    # Exit a parse tree produced by TimestampsParser#info.
    def exitInfo(self, ctx:TimestampsParser.InfoContext):
        pass


    # Enter a parse tree produced by TimestampsParser#timestamp.
    def enterTimestamp(self, ctx:TimestampsParser.TimestampContext):
        pass

    # Exit a parse tree produced by TimestampsParser#timestamp.
    def exitTimestamp(self, ctx:TimestampsParser.TimestampContext):
        pass


    # Enter a parse tree produced by TimestampsParser#label.
    def enterLabel(self, ctx:TimestampsParser.LabelContext):
        pass

    # Exit a parse tree produced by TimestampsParser#label.
    def exitLabel(self, ctx:TimestampsParser.LabelContext):
        pass


    # Enter a parse tree produced by TimestampsParser#interval.
    def enterInterval(self, ctx:TimestampsParser.IntervalContext):
        pass

    # Exit a parse tree produced by TimestampsParser#interval.
    def exitInterval(self, ctx:TimestampsParser.IntervalContext):
        pass


    # Enter a parse tree produced by TimestampsParser#time.
    def enterTime(self, ctx:TimestampsParser.TimeContext):
        pass

    # Exit a parse tree produced by TimestampsParser#time.
    def exitTime(self, ctx:TimestampsParser.TimeContext):
        pass



del TimestampsParser