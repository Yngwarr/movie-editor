# Generated from Timestamps.g4 by ANTLR 4.9.3
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
    from typing import TextIO
else:
    from typing.io import TextIO



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\16")
        buf.write("P\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\3\2")
        buf.write("\3\2\3\3\3\3\3\4\3\4\3\5\3\5\3\5\3\5\3\6\3\6\3\7\6\7)")
        buf.write("\n\7\r\7\16\7*\3\b\6\b.\n\b\r\b\16\b/\3\t\6\t\63\n\t\r")
        buf.write("\t\16\t\64\3\n\5\n8\n\n\3\13\3\13\7\13<\n\13\f\13\16\13")
        buf.write("?\13\13\3\13\3\13\3\13\3\13\3\f\5\fF\n\f\3\f\3\f\3\f\3")
        buf.write("\f\3\r\6\rM\n\r\r\r\16\rN\3=\2\16\3\3\5\4\7\5\t\6\13\7")
        buf.write("\r\b\17\t\21\n\23\13\25\f\27\r\31\16\3\2\b\4\2UUuu\3\2")
        buf.write("\62;\5\2C\\aac|\5\2\u0403\u0403\u0412\u0451\u0453\u0453")
        buf.write("\b\2##%(*\60??AB`a\4\2\13\13\"\"\2U\2\3\3\2\2\2\2\5\3")
        buf.write("\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2")
        buf.write("\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2")
        buf.write("\2\27\3\2\2\2\2\31\3\2\2\2\3\33\3\2\2\2\5\35\3\2\2\2\7")
        buf.write("\37\3\2\2\2\t!\3\2\2\2\13%\3\2\2\2\r(\3\2\2\2\17-\3\2")
        buf.write("\2\2\21\62\3\2\2\2\23\67\3\2\2\2\259\3\2\2\2\27E\3\2\2")
        buf.write("\2\31L\3\2\2\2\33\34\7/\2\2\34\4\3\2\2\2\35\36\7<\2\2")
        buf.write("\36\6\3\2\2\2\37 \7\60\2\2 \b\3\2\2\2!\"\7g\2\2\"#\7p")
        buf.write("\2\2#$\7f\2\2$\n\3\2\2\2%&\t\2\2\2&\f\3\2\2\2\')\t\3\2")
        buf.write("\2(\'\3\2\2\2)*\3\2\2\2*(\3\2\2\2*+\3\2\2\2+\16\3\2\2")
        buf.write("\2,.\t\4\2\2-,\3\2\2\2./\3\2\2\2/-\3\2\2\2/\60\3\2\2\2")
        buf.write("\60\20\3\2\2\2\61\63\t\5\2\2\62\61\3\2\2\2\63\64\3\2\2")
        buf.write("\2\64\62\3\2\2\2\64\65\3\2\2\2\65\22\3\2\2\2\668\t\6\2")
        buf.write("\2\67\66\3\2\2\28\24\3\2\2\29=\7%\2\2:<\13\2\2\2;:\3\2")
        buf.write("\2\2<?\3\2\2\2=>\3\2\2\2=;\3\2\2\2>@\3\2\2\2?=\3\2\2\2")
        buf.write("@A\5\27\f\2AB\3\2\2\2BC\b\13\2\2C\26\3\2\2\2DF\7\17\2")
        buf.write("\2ED\3\2\2\2EF\3\2\2\2FG\3\2\2\2GH\7\f\2\2HI\3\2\2\2I")
        buf.write("J\b\f\2\2J\30\3\2\2\2KM\t\7\2\2LK\3\2\2\2MN\3\2\2\2NL")
        buf.write("\3\2\2\2NO\3\2\2\2O\32\3\2\2\2\n\2*/\64\67=EN\3\2\3\2")
        return buf.getvalue()


class TimestampsLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    T__0 = 1
    T__1 = 2
    T__2 = 3
    T__3 = 4
    SOLO = 5
    INT = 6
    ID = 7
    CYRILLIC = 8
    PUNCTUATION = 9
    COMMENT = 10
    NEWLINE = 11
    WS = 12

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'-'", "':'", "'.'", "'end'" ]

    symbolicNames = [ "<INVALID>",
            "SOLO", "INT", "ID", "CYRILLIC", "PUNCTUATION", "COMMENT", "NEWLINE", 
            "WS" ]

    ruleNames = [ "T__0", "T__1", "T__2", "T__3", "SOLO", "INT", "ID", "CYRILLIC", 
                  "PUNCTUATION", "COMMENT", "NEWLINE", "WS" ]

    grammarFileName = "Timestamps.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.9.3")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


