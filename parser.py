from antlr4 import *

from TimestampsLexer import TimestampsLexer
from TimestampsParser import TimestampsParser
from TimestampsListener import TimestampsListener

class Timestamps(object):
    def __init__(self):
        self.cmds = []
        self.has_solo = False

    def __repr__(self):
        return f'(has_solo={self.has_solo}, cmds={self.cmds})'

    def to_compose(self):
        return [cmd for cmd in self.cmds if cmd.solo] if self.has_solo else self.cmds

class TimestampCmd(object):
    def __init__(self, t0, t1, solo, label):
        self.t0 = t0
        self.t1 = t1
        self.solo = solo
        self.label = label

    def __str__(self):
        label = '' if self.label == None else f' "{self.label}"'
        return f"{'S ' if self.solo else ''}{self.t0} - {self.t1}{label}"

    def __repr__(self):
        return f'({str(self)})'

class Listener(TimestampsListener):
    def __init__(self):
        self.ts = Timestamps()

    def exitTimestamp(self, ctx):
        time = ctx.interval().time()
        label = ctx.label()
        solo = ctx.SOLO() != None

        if solo:
            self.ts.has_solo = True

        self.ts.cmds.append(TimestampCmd(
            time[0].getText(), time[1].getText(),
            solo, None if label == None else label.getText()))

def parseTimestamps(filename):
    stream = FileStream(filename, 'utf-8')
    lexer = TimestampsLexer(stream)
    token_stream = CommonTokenStream(lexer)
    parser = TimestampsParser(token_stream)

    tree = parser.info()
    walker = ParseTreeWalker()
    listener = Listener()
    walker.walk(listener, tree)

    return listener.ts

if __name__ == '__main__':
    import sys
    from antlr4.InputStream import InputStream

    input_stream = InputStream(sys.stdin.read())
    lexer = TimestampsLexer(input_stream)
    token_stream = CommonTokenStream(lexer)
    parser = TimestampsParser(token_stream)

    tree = parser.info()

    walker = ParseTreeWalker()
    listener = Listener()
    walker.walk(listener, tree)

    print(tree.toStringTree(recog=parser))
    print(listener.cmds)
